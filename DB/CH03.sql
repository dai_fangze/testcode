#https://db-engines.com/en/ranking
#http://sqlfiddle.com/
CREATE TABLE Student          
	      (Sno   CHAR(9) PRIMARY KEY,          
            Sname  CHAR(20) UNIQUE,    /* Sname取唯一值*/
            Ssex    CHAR(2),
            Sage   SMALLINT,
            Sdept  CHAR(20)
           ); 
CREATE TABLE  Course
               ( Cno       CHAR(4) PRIMARY KEY,
                 Cname  CHAR(40),            
                 Cpno     CHAR(4),              	                      
                 Ccredit  SMALLINT,
                FOREIGN KEY (Cpno) REFERENCES  Course(Cno) 
            ); 

CREATE TABLE  SC
       	(Sno  CHAR(9),
       	Cno  CHAR(4),  
       	Grade    SMALLINT,
       	PRIMARY KEY (Sno,Cno),  
                     /* 主码由两个属性构成，必须作为表级完整性进行定义*/
       	FOREIGN KEY (Sno) REFERENCES Student(Sno),
                    /* 表级完整性约束条件，Sno是外码，被参照表是Student */
       	FOREIGN KEY (Cno) REFERENCES Course(Cno)
                   /* 表级完整性约束条件， Cno是外码，被参照表是Course*/
		); 
        
        
 /*Student 数据*/       
/*INSERT INTO Student(Sno,Sname,Ssex,Sdept,Sage) VALUES('201215128','陈东','男','IS',18);*/
INSERT INTO Student  VALUES('201215121','李勇','男',20,'CS');
INSERT INTO Student  VALUES('201215122','刘晨','女',19,'CS');
INSERT INTO Student  VALUES('201215123','王敏','女',18,'MA');
INSERT INTO Student  VALUES('201215125','张立','男',19,'IS');

/* Course数据*/
INSERT INTO Course  VALUES('2','数学',NULL,2);
INSERT INTO Course  VALUES('6','数据处理',NULL,2);
INSERT INTO Course  VALUES('4','操作系统','6',3);
INSERT INTO Course  VALUES('7','PASCAL语言','6',4);
INSERT INTO Course  VALUES('5','数据结构','7',4);
INSERT INTO Course  VALUES('1','数据库','5',4);
INSERT INTO Course  VALUES('3','信息系统','1',4);

/* SC数据*/
INSERT INTO SC  VALUES('201215121','1',92);
INSERT INTO SC  VALUES('201215121','2',85);
INSERT INTO SC  VALUES('201215121','3',88);
INSERT INTO SC  VALUES('201215122','2',90);
INSERT INTO SC  VALUES('201215122','3',80);


/*3.4 数据查询*/

/*3.4.1 单标查询*/

   /*1-选择若干列*/
#例-3.16
SELECT Sno,Sname
FROM Student;

#例-3.17
SELECT Sname,Sno,Sdept
FROM Student;

#例-3.18
SELECT *
FROM Student;

#例-3.19
SELECT Sname,2014-Sage
FROM Student;

#例-3.20
SELECT Sname,'Year of Birth: ',2014-Sage, LOWER(Sdept)
FROM Student;

SELECT Sname NAME,'Year of Birth: '  BIRTH, 2014-Sage  BIRTHDAY,
        LOWER(Sdept)  DEPARTMENT
FROM Student;
    
     /*查询若干元组*/

#例-3.21
SELECT Sno
FROM SC;

SELECT DISTINCT Sno
FROM SC;

/*条件查询*/
#例-3.22
SELECT Sname
FROM Student
WHERE Sdept='CS';

#例-3.23
SELECT Sname,Sage
FROM Student
WHERE Sage<20;

#例-3.24
SELECT DISTINCT Sno
FROM SC
WHERE Grade<60;

#例-3.25
SELECT Sname,Sdept,Sage
FROM Student
WHERE Sage BETWEEN 20 AND 23;

#例-3.26
SELECT Sname,Sdept,Sage
FROM Student
WHERE Sage NOT BETWEEN 20 AND 23;

#例3.27：查询系CS、MA、IS的学生的姓名和性别（谓词IN的使用，不在就用NOT IN）
SELECT Sname,Ssex
FROM Student
WHERE Sdept IN('CS','MA','IS');
/*WHERE Sdept='CS' OR Sdept='MA' OR Sdept='IS';*/

#例-3.28
SELECT Sname,Ssex
FROM Student
WHERE Sdept NOT IN('CS','MA','IS');

#例-3.29：查询学号为201215121的学生的详细情况（开始涉及到字符匹配）
SELECT *
FROM Student
WHERE Sno LIKE '201215121';/*等于WHERE Sno='201215121'*/

#例-3.30：查询所有性刘的学生的姓名、学号和性别（%的使用）
SELECT Sname,Sno,Ssex
FROM Student
WHERE Sname LIKE '刘%';

#例-3.31:查询姓“欧阳”且全名为3个汉字的学生的姓名
#（_的使用，数据库字符集为ASCII时一个汉字需要两个_，当字符集为GBK时只需要一个）
SELECT Sname
FROM Student
WHERE Sname LIKE '欧阳_';

#例-3.32
SELECT Sname
FROM Student
WHERE Sname LIKE '_阳%';

#例-3.33
SELECT Sname
FROM Student
WHERE Sname NOT LIKE '_阳%';

#例-3.34
SELECT Cno,Ccredit
FROM Course
WHERE Cname LIKE 'DB\_Design' ESCAPE '\';

#例-3.35
SELECT *
FROM Course
WHERE Cname LIKE 'DB\_%i__' ESCAPE '\';
/**/
#例-3.36
SELECT Sno,Cno
FROM SC
WHERE Grade is NULL;/*如果是非空就用NOT NULL*/

#例-3.37
SELECT Sno,Cno
FROM SC
WHERE Grade is NOT NULL;

#例-3.38
SELECT Sname
FROM Student
WHERE Sdept='CS' AND Sage<20;

#例-3.39
SELECT Sno,Grade
FROM SC
WHERE Cno='3'
ORDER BY Grade DESC;

#例-3.40
SELECT *
FROM Student
ORDER BY Sdept,Sage DESC;

#例-3.41
SELECT COUNT(*)
FROM Student;

#例-3.42
SELECT COUNT(DISTINCT Sno)
FROM SC;

#例-3.43
SELECT AVG(Grade)
FROM SC
WHERE Cno='1';

#例-3.44
SELECT MAX(Grade)
FROM SC
WHERE Cno='1';

#例-3.45
SELECT SUM(Ccredit)
FROM SC,Course
WHERE Sno='201215012' AND SC.Cno=Course.Cno;

#例-3.46
SELECT Cno,COUNT(Sno)
FROM SC
GROUP BY Cno;/*所有具有相同Cno的人为一组*/

#例-3.47
SELECT Sno
FROM SC
GROUP BY Sno/*先用GROUP BY子句按Sno进行分组*/
HAVING COUNT(*)>3;
/*再用聚集函数COUNT对每一组进行计数，HAVING短语给出了选择组的条件*/
/*WHERE子句中是不能用聚集函数作为条件表达式的*/

#例-3.48
/* 错误语句
SELECT Sno,AVG(Grade)
FROM SC
WHERE AVG(Grade)>=90
GROUP BY Sno
*/
SELECT Sno,AVG(Grade)
FROM SC
GROUP BY Sno
HAVING AVG(Grade)>=90;

/******************************连接查询************************************/

#例-3.49
SELECT student.*,sc.* /*注意表名后面的那个. */
FROM student,sc
WHERE student.Sno=sc.Sno;

#例-3.50
SELECT student.Sno,Sname,Ssex,Sage,Sdept,Cno,Grade
FROM student,sc
WHERE student.Sno=sc.Sno;

#例-3.51
SELECT student.Sno,Sname
FROM student,sc
WHERE student.Sno=sc.Sno AND sc.Cno='2' AND sc.Grade>90;

#例-3.52
SELECT FIRST.Cno,SECOND.Cno
FROM course FIRST,course SECOND
WHERE FIRST.Cpno=SECOND.Cno;

#例-3.53
SELECT student.Sno,Sname,Ssex,Sage,Sdept,Cno,Grade
FROM student LEFT OUTER JOIN sc ON (student.Sno=sc.Sno);

#例-3.54
SELECT student.Sno,Sname,Cname,Grade
FROM student,sc,course
WHERE student.Sno=sc.Sno AND sc.Cno=course.Cno;/*连接三个表*/

/**************************嵌套查询******************************/
#例-3.55
SELECT Sno,Sname,Sdept
FROM student
WHERE Sdept IN 
        (
		SELECT Sdept
		FROM student
		WHERE Sname = '刘晨'
	);


SELECT S1.Sno,S1.Sname,S1.Sdept
FROM student S1,student S2
WHERE S1.Sdept=S2.Sdept AND S2.Sname='刘晨';

#例-3.56
SELECT Sno,Sname/*③最后根据学号获取信息*/
FROM student
WHERE Sno IN (/*②然后在SC表找出选修该课程号的学生的学号*/
		SELECT Sno
		FROM sc
		WHERE Cno IN (/*①首先在Course中找出“信息系统”的课程号*/
				SELECT Cno
				FROM course
				WHERE Cname = '信息系统'
			)
	);

/**等价的连接查询*/
SELECT Student.Sno,Sname
FROM Student,SC,Course
WHERE Student.Sno=SC.Sno AND
        SC.Cno=Course.Cno AND
        Course.Cname='信息系统';


#例-3.57
SELECT Sno,Cno
FROM sc x/*别名*/
WHERE Grade>=( SELECT AVG(Grade)
		FROM sc y
		WHERE y.Sno=x.Sno);

#例-3.58
SELECT Sname,Sage
FROM student
WHERE Sage<ANY( SELECT Sage
		FROM student
		WHERE Sdept='CS')
AND Sdept<>'CS';

#例-3.59
SELECT Sname,Sage
FROM student
WHERE Sage<ALL( SELECT Sage
		FROM student
		WHERE Sdept='CS')
AND Sdept<>'CS';

#例-3.60
SELECT Sname
FROM student
WHERE EXISTS
    (SELECT*
    FROM sc
    WHERE Sno=student.Sno AND Cno='1');

#例-3.61
SELECT Sname
FROM student
WHERE NOT EXISTS
    (SELECT*
    FROM sc
    WHERE Sno=student.Sno AND Cno='1');

#例-3.62
select Sname
from Student
where Sno  in
    (select Sno
    from SC
    group by Sno --根据Sno分组，统计每个学生选修了几门课程。如果等于C表课程的总数，就是我们要找的S#
    having count(*) = (select count(*) from Course))--统计C表中共有几门课程
#######################
SELECT Sname
FROM student
WHERE NOT EXISTS (
		SELECT *
		FROM course
		WHERE NOT EXISTS (
				SELECT *
				FROM sc
				WHERE Sno = student.Sno
				AND Cno = course.Cno
			)
	);

#例-3.63
SELECT DISTINCT Sno
FROM sc SCX
WHERE NOT EXISTS
        (SELECT *
        FROM sc SCY
        WHERE SCY.Sno='201215122' AND
        NOT EXISTS
                (SELECT *
                FROM sc SCZ
                WHERE SCZ.Sno=SCX.Sno AND
                SCZ.Cno=SCY.Cno));

#例-3.64
SELECT *
FROM student
WHERE Sdept='CS'
UNION
SELECT *
FROM student
WHERE Sage<=19;
 
/*或者*/
SELECT *
FROM student
WHERE Sdept='CS' OR Sage<=19;

#例-3.65

#例-3.66
SELECT *
FROM student
WHERE Sdept='CS'
INTERSECT
SELECT *
FROM student
WHERE Sage<=19;
 
/*或者*/
SELECT *
FROM student
WHERE Sdept='CS' AND Sage<=19;

#例-3.67

#例-3.68
SELECT *
FROM student
WHERE Sdept='CS'
EXCEPT
SELECT *
FROM student
WHERE Sage<=19;
 
/*或者*/
SELECT *
FROM student
WHERE Sdept='CS' AND Sage>19;


SELECT Sname
FROM student,(SELECT Sno FROM sc WHERE Cno='1')AS SC1/*一定要写别名*/
WHERE student.Sno=SC1.Sno;


#例-3.69
INSERT
INTO student(Sno,Sname,Ssex,Sdept,Sage)
VALUES('201215128','陈冬','男','IS',18);

#例-3.70
INSERT
INTO student
VALUES('201215126','张成民','男',18,'CS');
#例-3.71
INSERT
INTO sc(Sno,Cno)
VALUES('201215128','1');
 
/*等同于*/
INSERT
INTO sc
VALUES('201215128','1',NULL);

#例-3.72
CREATE TABLE Dept_age
(Sdept CHAR(15),
Avg_age SMALLINT);
/*再分组*/
INSERT
INTO Dept_age(Sdept,Avg_age)
SELECT Sdept,AVG(sage)
from student
GROUP BY Sdept;

#例-3.73
UPDATE student
SET Sage=22
WHERE Sno='201215121';

#例-3.74
UPDATE student
SET Sage=Sage+1;

#例-3.75
UPDATE sc
SET Grade=0
WHERE Sno IN
(SELECT Sno
FROM student
WHERE Sdept='CS');

#例-3.76
DELETE 
FROM student
WHERE Sno='201215128';

#例-3.77
DELETE 
FROM sc;

#例-3.78
DELETE 
FROM sc
WHERE Sno IN
(SELECT Sno
FROM student
WHERE Sdept='CS');

#例-3.79
#例-3.80
#例-3.81
SELECT*
FROM student
WHERE Sname IS NULL OR Ssex IS NULL OR Sage IS NULL OR Sdept IS NULL;

#例-3.83
SELECT Sno
FROM sc
WHERE Grade<60 AND Cno='1'
UNION
SELECT Sno
FROM sc
WHERE Grade IS NULL AND Cno='1';
 
/*等于*/
SELECT Sno
FROM sc
WHERE Cno='1' AND (Grade<60 OR Grade IS NULL);

#例-3.84
CREATE VIEW IS_Student
AS
SELECT Sno,Sname,Sage
FROM student
WHERE Sdept='IS'

#例-3.85
CREATE VIEW IS_Student
AS
SELECT Sno,Sname,Sage
FROM student
WHERE Sdept='IS'
WITH CHECK OPTION;

#例-3.86
CREATE VIEW IS_S1(Sno,Sname,Grade)
AS
SELECT student.Sno,Sname,Grade
FROM student,sc
WHERE Sdept='IS'AND
	student.Sno=sc.Sno AND
        sc.Cno='1';

#例-3.87
CREATE VIEW IS_S2
AS
SELECT Sno,Sname,Grade
FROM IS_S1/*信息系选修了1号课程的学生*/
WHERE Grade>90;

#例-3.88
CREATE VIEW BT_S(Sno,Sname,Sbirth)
AS
SELECT Sno,Sname,2014-Sage
FROM Student;

#例-3.89
CREATE VIEW S_G(Sno,Gavg)
AS
SELECT Sno,AVG(Grade)
From SC 
GROUP BY Sno;

#例-3.90
CREATE VIEW F_Student(F_sno,name,sex,age,dept)
AS
select *
from student
WHERE Ssex = '女';

#例-3.91
DROP VIEW BT_S;/*成功执行*/
DROP VIEW IS_S1;/*拒绝执行*/
/*由于IS_S1还定义了视图IS_S2，所以要采用级联删除*/
DROP VIEW IS_S1 CASCADE;

#例-3.92
SELECT Sno,Sage
FROM is_student
WHERE Sage<20;

SELECT Sno,Sage
FROM student
WHERE Sdept='IS' AND Sage<20;


#例-3.94
SELECT *
FROM S_G
WHERE Gavg>=90;

SELECT Sno,AVG(Grade)
FROM SC
WHERE AVG(Grade>=90)
GROUP BY Sno;

SELECT Sno,AVG(Grade)
FROM SC
GROUP BY Sno
HAVING AVG(Grade>=90);

SELECT *
FROM (SELECT Sno,AVG(Grade)
      FROM sc
      GROUP BY Sno)AS S_G(Sno,Gavg)
WHERE Gavg>=90;

#例-3.95
UPDATE IS_Student 
SET Sname='刘辰'
WHERE Sno='201215122';

UPDATE Student
SET Sname='刘辰'
WHERE Sno='201215122' AND Sdept='IS';


#例-3.96
INSERT
INTO IS_Student
VALUES('95029'，'赵新'，20);

INSERT
INTO Student(sno,Sname,sage,Sdept)
VALUES('95029'，'赵新'，20,'IS');

#例-3.97
DELETE 
FROM is_student
WHERE Sno='201215129';
/*转换后*/
DELETE 
FROM Student 
WHERE Sno='201215129' AND Sdept='IS';

#例-3.9
#例-3.9





